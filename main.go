package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func main() {
	serverPort := ""
	if value, ok := os.LookupEnv(`SERVER_PORT`); ok {
		serverPort = value
	} else {
		serverPort = "8080"
	}
	http.HandleFunc("/upload", fileUpload)
	log.Printf("Starting server on port :%s", serverPort)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", serverPort), nil); err != nil {
		log.Fatal(err)
	}
}

func fileUpload(w http.ResponseWriter, r *http.Request) {
	maxFileSize := getMaxFileSize(`MAX_FILE_SIZE`, 32)
	log.Println("File upload started")
	_ = r.ParseMultipartForm(maxFileSize << 20)
	file, handler, err := r.FormFile("file") // Retrieve the file from form data
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer file.Close()
	storePath := checkStorePath(`STORE_PATH`, "/tmp/")
	fileName := strings.Split(handler.Filename, "/")
	f, err := os.OpenFile(storePath+fileName[len(fileName)-1], os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
		return
	}
	_, err = io.Copy(f, file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("File saved"))
	log.Println("File saved")
}

func getMaxFileSize(key string, fallback int64) int64 {
	if value, ok := os.LookupEnv(key); ok {
		parsedInt, err := strconv.ParseInt(value, 10, 64)
		if err != nil {
			log.Printf("env var %s cannot be converted to integer, falling back to default", key)
			return fallback
		}
		return parsedInt
	}
	return fallback
}

func checkStorePath(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		if _, err := os.Stat(os.Getenv(`STORE_PATH`)); err != nil {
			log.Println("provided path does not exists, using /tmp")
			return fallback
		}
		return value
	}
	return fallback
}
