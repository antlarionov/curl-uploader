Service listens on http port and saves file to local folder.

Example: ` curl -F 'file=@some-file' http://localhost:8080/ -vvv`

| Env var | Type | Description | Default |
| ----- | ----------- | ------ |------|
| `SERVER_PORT` | int | http server port to serve on | 8080 |
| `MAX_FILE_SIZE` | int | upload file max size in megabytes | 32 mb |
| `STORE_PATH` | string |path where file should be saved. Must end with `/` | `/tmp/` | 
 
