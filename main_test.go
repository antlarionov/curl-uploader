package main

import (
	"bytes"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strconv"
	"testing"
)

func TestGetFileSizeDefaultValue(t *testing.T) {
	defaultValue := int64(32)
	variableName := "MAX_FILE_SIZE"

	if getMaxFileSize(variableName, defaultValue) != defaultValue {
		t.Fail()
	}
}

func TestGetStorePathDefaultValue(t *testing.T) {
	defaultValue := "/tmp/"
	variableName := "STORE_PATH"

	if checkStorePath(variableName, defaultValue) != defaultValue {
		t.Fail()
	}
}

func TestGetFileSizeDefaultValueFromENV(t *testing.T) {
	defaultValue := int64(32)
	variableName := "MAX_FILE_SIZE"
	originValue := "64"
	if err := os.Setenv(variableName, originValue); err != nil {
		log.Fatal(err)
	}
	parsedEnvInt, err := strconv.ParseInt(originValue, 10, 64)
	if err != nil {
		log.Fatal(err)
	}
	if getMaxFileSize(variableName, defaultValue) != parsedEnvInt {
		t.Fail()
	}
}

func TestGetStorePathValueFromENV(t *testing.T) {
	defaultValue := "/tmp/"
	variableName := "STORE_PATH"
	originValue := os.Getenv(`PWD`)
	if err := os.Setenv(variableName, originValue); err != nil {
		log.Fatal(err)
	}

	if checkStorePath(variableName, defaultValue) != originValue {
		t.Fail()
	}
}

func TestUpload(t *testing.T) {
	path := "README.md" //The path to upload the file
	file, err := os.Open(path)
	if err != nil {
		t.Error(err)
	}

	defer file.Close()
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	if err != nil {
		_ = writer.Close()
		t.Error(err)
	}
	_, _ = io.Copy(part, file)
	_ = writer.Close()

	req := httptest.NewRequest("POST", "/upload", body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res := httptest.NewRecorder()

	fileUpload(res, req)

	if res.Code != http.StatusOK {
		t.Error("not 200")
	}

	t.Log(res.Body.String())
	// t.Log(io.read)

}
