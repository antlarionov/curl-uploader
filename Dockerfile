FROM golang:alpine as builder
RUN mkdir /src
ADD . /src
RUN cd /src && go build -o curl-uploader

FROM alpine
WORKDIR /app
COPY --from=builder /src/curl-uploader /app
ENTRYPOINT ./curl-uploader
